package com.bazar.dao;

import java.util.List;

import com.bazar.model.Order;


public interface OrderDao {
	public void addOrder(Order order);		
	
	public List<Order> listOrderss();
}
