package com.bazar.dao;

import java.util.List;

import com.bazar.model.Product;

public interface ProductDao {
	
	public void addProduct(Product product);		
	public Product getProduct(double barcode);
	public void deleteProduct(Product product);
	public List<Product> listProductss();
 // public List<vProduct> listvProductss();
	
}
