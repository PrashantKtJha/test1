package com.bazar.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bazar.model.Order;
import com.bazar.model.Product;



@Repository("productDao")
public class ProductDaoImpl implements ProductDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addProduct(Product product) {
		sessionFactory.getCurrentSession().saveOrUpdate(product);
		}

	@SuppressWarnings("unchecked")
	public List<Product> listProductss() {
		
		return (List<Product>) sessionFactory.getCurrentSession().createCriteria(Product.class).list();
	
	}

	@Override
	public Product getProduct(double barcode) {
		return (Product) sessionFactory.getCurrentSession().get(Product.class, barcode);
	}

	@Override
	public void deleteProduct(Product product) {
		sessionFactory.getCurrentSession().createQuery("DELETE FROM Employee WHERE empid = "+product.getBarcode()).executeUpdate();
	}

	

}
