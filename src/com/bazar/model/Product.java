package com.bazar.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="Product")
public class Product {

	
	
	@Id
	@Column(name = "barcode")
	private double barcode;
	
	@Column(name="name")
	private String name;
	
	
	@Column(name="price")
	private double price;


	public void setBarcode(double barcode) {
		this.barcode = barcode;
	}



	public void setName(String name) {
		this.name = name;
	}



	public void setPrice(double price) {
		this.price = price;
	}



	public double getBarcode() {
		return barcode;
	}


	
	public String getName() {
		return name;
	}


	

	public double getPrice() {
		return price;
	}


	
	
	
	
}
