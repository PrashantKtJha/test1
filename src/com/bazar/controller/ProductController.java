package com.bazar.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.bazar.bean.ProductBean;
import com.bazar.model.Order;
import com.bazar.model.Product;
import com.bazar.service.ProductService;

@Controller
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView saveProduct(@ModelAttribute("command") ProductBean productBean, 
			BindingResult result) {
		Product product = prepareModel(productBean);
		productService.addProduct(product);
		return new ModelAndView("redirect:/add.html");
	}

	@RequestMapping(value="/products", method = RequestMethod.GET)
	public ModelAndView listProducts() {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("products",  prepareListofBean(productService.listProductss()));
		return new ModelAndView("productsList", model);
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public ModelAndView addProduct(@ModelAttribute("command")  ProductBean productBean,
			BindingResult result) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("products",  prepareListofBean(productService.listProductss()));
		return new ModelAndView("addProduct", model);
	}
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView welcome() {
		return new ModelAndView("index");
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView editProduct(@ModelAttribute("command")  ProductBean productBean,
			BindingResult result) {
		productService.deleteProduct(prepareModel(productBean));
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("product", null);
		model.put("products",  prepareListofBean(productService.listProductss()));
		return new ModelAndView("addProduct", model);
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView deleteProduct(@ModelAttribute("command")  ProductBean productBean,
			BindingResult result) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("product", prepareProductBean(productService.getProduct(productBean.getBarcode())));
		model.put("products",  prepareListofBean(productService.listProductss()));
		return new ModelAndView("addProduct", model);
	}
	
	private Product prepareModel(ProductBean productBean){
		Product product = new Product();
		
		product.setPrice(productBean.getPrice());
		product.setBarcode(productBean.getBarcode());
		product.setName(productBean.getName());
		
		//productBean.setBarcode(null);
		return product;
	}
	
	private List<ProductBean> prepareListofBean(List<Product> products){
		List<ProductBean> beans = null;
		if(products != null && !products.isEmpty()){
			beans = new ArrayList<ProductBean>();
			ProductBean bean = null;
			for(Product product : products){
				
				bean = new ProductBean();
				bean.setName(product.getName());
				bean.setBarcode(product.getBarcode());
		
				bean.setPrice(product.getPrice());	
				//fetching data from database
				//bean.setProductName("micromax");
				//bean.setProductPrice(99999);
				SessionFactory factory1 = null;
				
				Session session1 = factory1.openSession();
				Criteria crit1 = session1.createCriteria(Order.class);
				//Integer i1=(Integer)(product.getBarcode());
				Criterion criterion = Restrictions.eq("productBarcode",product.getBarcode());
				crit1.add(criterion);
				java.util.List list = crit1.list();
			//	Iterator it=list.iterator()
				//while(it.hasNext())
				String s1=(String)list.get(2);
				Integer s2=(Integer)list.get(3);
				
			    bean.setProductName(s1);
			    bean.setProductPrice(s2);
				
				//java.util.List list = crit2.list();
				beans.add(bean);
				
				
			}
		}
		return beans;
	}
	
	private ProductBean prepareProductBean(Product product){
		ProductBean bean = new ProductBean();
		bean.setPrice(product.getPrice());
		bean.setBarcode(product.getBarcode());
		bean.setName(product.getName());
		return bean;
	}
}
