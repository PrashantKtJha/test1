package com.bazar.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bazar.dao.ProductDao;
import com.bazar.model.Product;


@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductDao productDao;
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addProduct(Product product) {
		productDao.addProduct(product);
	}
	
	public List<Product> listProductss() {
		return productDao.listProductss();
	}

	public Product getProduct(double barcode) {
		return productDao.getProduct(barcode);
	}
	
	public void deleteProduct(Product product) {
		productDao.deleteProduct(product);
	}
	}

	

	


