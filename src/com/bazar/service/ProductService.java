package com.bazar.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.bazar.model.Product;

@Service
public interface ProductService {
	
	public void addProduct(Product product);
    public List<Product> listProductss();
	
		public void deleteProduct(Product product);

	public Product getProduct(double barcode);
	
	
}
