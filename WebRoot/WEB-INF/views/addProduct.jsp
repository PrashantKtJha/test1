<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Bazar Apps</title>
	</head>
	<body>
		<h2>plz enter order information  </h2>
		<form:form method="POST" action="/sdnext/save.html">
	   		<table>
			    <tr>
			        <td><form:label path="barcode">Order Id:</form:label></td>
			        <td><form:input path="barcode" value="${Product.barcode}" readonly="true"/></td>
			    </tr>
			    <tr>
			        <td><form:label path="name">Customer Id:</form:label></td>
			        <td><form:input path="name" value="${Product.name}"/></td>
			    </tr>
			    <tr>
			        <td><form:label path="price">Product Barcode:</form:label></td>
			        <td><form:input path="price" value="${product.price}"/></td>
			    </tr>
			    
			    <tr>
			      <td colspan="2"><input type="submit" value="Submit"/></td>
		      </tr>
			</table> 
		</form:form>
		
  <c:if test="${!empty products}">
		<h2>your order t</h2>
	<table align="left" border="1">
		<tr>
			<th>Order Id </th>
			<th>Customer Id</th>
			<th>Barcode</th>
			<th>Product Name </th>
			<th>Product Price</th>
			
			
			<th>Actions on Row</th>
		</tr>

		<c:forEach items="${products}" var="product">
			<tr>
				<td><c:out value="${product.barcode}"/></td>
				<td><c:out value="${product.name}"/></td>
				<td><c:out value="${product.price}"/></td>
				
				<td><c:out value="${product.productName}"/></td>
				<td><c:out value="${product.productPrice}"/></td>
				
				<td align="center"><a href="edit.html?id=${product.barcode}">Edit</a> | <a href="delete.html?id=${product.barcode}">Delete</a></td>
			</tr>
		</c:forEach>
	</table>
</c:if> 






</table>


	</body>
</html>