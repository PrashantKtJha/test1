<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>all order</title>
</head>
<body>
<h1>List order</h1>
<h3><a href="add.html">More order</a></h3>

<c:if test="${!empty products}">
	<table align="left" border="1">
		<tr>
			<th>Product Barcode</th>
			<th>Product Name</th>
			<th>Product Proce</th>
			
		</tr>

		<c:forEach items="${products}" var="product">
			<tr>
				<td><c:out value="${product.barecode}"/></td>
				<td><c:out value="${product.name}"/></td>
				<td><c:out value="${product.price}"/></td>
				
			</tr>
		</c:forEach>
	</table>
</c:if>
</body>
</html>